from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from ResultUi import Ui_ResultUi


class StatsWidget(QWidget):
    """
    Wiget odpowiedzialny za wyświetlanie statystyk dopasowania
    """
    def __init__(self, base=None):
        QWidget.__init__(self, base)
        self.ui = Ui_ResultUi()
        self.ui.setupUi(self)


    def updateResults(self,name,genName,phi,determin):
        """
        Slot aktualizujacy dane
        :param name: nazwa funkcji modelowej
        :param genName: nazwa funkcji generujacej
        :param phi: wartość testu X^2
        :param determin: wartość testu R^2
        :return:
        """
        self.ui.modelName.setText(name)
        self.ui.phiValue.setText(str(phi))
        self.ui.generatorName.setText(genName)
        self.ui.determinRatio.setText(str(determin))