# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sinusmodelui.ui'
#
# Created: Mon Apr 18 21:18:56 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_sinusModelUi(object):
    def setupUi(self, sinusModelUi):
        sinusModelUi.setObjectName("sinusModelUi")
        sinusModelUi.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(sinusModelUi)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtWidgets.QLabel(sinusModelUi)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.label = QtWidgets.QLabel(sinusModelUi)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label_3 = QtWidgets.QLabel(sinusModelUi)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.label_4 = QtWidgets.QLabel(sinusModelUi)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.label_5 = QtWidgets.QLabel(sinusModelUi)
        self.label_5.setObjectName("label_5")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.a_param = QtWidgets.QDoubleSpinBox(sinusModelUi)
        self.a_param.setMinimum(-10000.0)
        self.a_param.setMaximum(10000.0)
        self.a_param.setObjectName("a_param")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.a_param)
        self.b_param = QtWidgets.QDoubleSpinBox(sinusModelUi)
        self.b_param.setMinimum(-10000.0)
        self.b_param.setMaximum(10000.0)
        self.b_param.setObjectName("b_param")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.b_param)
        self.c_param = QtWidgets.QDoubleSpinBox(sinusModelUi)
        self.c_param.setMinimum(-10000.0)
        self.c_param.setMaximum(10000.0)
        self.c_param.setObjectName("c_param")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.c_param)
        self.verticalLayout.addLayout(self.formLayout)
        self.apply_button = QtWidgets.QPushButton(sinusModelUi)
        self.apply_button.setObjectName("apply_button")
        self.verticalLayout.addWidget(self.apply_button)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)

        self.retranslateUi(sinusModelUi)
        QtCore.QMetaObject.connectSlotsByName(sinusModelUi)

    def retranslateUi(self, sinusModelUi):
        _translate = QtCore.QCoreApplication.translate
        sinusModelUi.setWindowTitle(_translate("sinusModelUi", "Dialog"))
        self.label_2.setText(_translate("sinusModelUi", "<b>Select parameters:</b>"))
        self.label.setText(_translate("sinusModelUi", "y=a*sin(bx+c)"))
        self.label_3.setText(_translate("sinusModelUi", "a"))
        self.label_4.setText(_translate("sinusModelUi", "b"))
        self.label_5.setText(_translate("sinusModelUi", "c"))
        self.apply_button.setText(_translate("sinusModelUi", "Apply"))

