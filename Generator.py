from random import *

class Generator:
    @staticmethod
    def generateRandom(pointerToFunction,params,fromValue, toValue, samples,sigma):
        """
        Generuje losowy rozkład oparty o zadany model
        Argumenty:
            pointerToFunction - wskaźnik na funkcję obliczajaca pochodzaca z modelu
            params - parametry tej funkcji (krotka/lista)
            fromValue - minimalna wartość argumentu
            toValue - maksymalna wartość argumentu
            samples - ilość próbek ( wygenerowanych punktów)
            sigma - odchylenie standardowe od funkcji modelu
        Zwraca:
            x - lista argumentów puntków
            result - lista wartości puntków
        """
        x=[random()*(toValue-fromValue)+fromValue for i in range(0,samples)]

        y=pointerToFunction(x,*params)
        yError=[gauss(0,sigma) for i in range(0,samples)]

        result=[y[i]+yError[i] for i in range(0,samples)]
        return (x,result)
