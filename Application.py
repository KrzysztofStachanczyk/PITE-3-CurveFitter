import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from MainUi import Ui_MainUi
from SinusModelDialog import SinusModelDialog
from LogModelDialog import LogModelDialog
from LinearModelDialog import LinearModelDialog
from Generator import Generator
import ModelFunctions
from StatAnalyser import StatAnalyser
from Fitter import Fitter


class Application(QWidget):
    """
    Główna klasa aplikacji.
    """
    def __init__(self, base=None):
        """
        Konstruktor inicjujący UI
        """
        QWidget.__init__(self, base)
        self.ui = Ui_MainUi()
        self.ui.setupUi(self)
        self.setWindowTitle("Curve fitter")
        self.ui.apply_button.clicked.connect(self.applyRequest)


    def applyRequest(self):
        """
        Slot odpowiedzialny za przetworzenie rządania symulacji wykrywania
        dopasowania modelu do danych - w oparciu o zadane w polach UI parametry
        """
        fromValue=self.ui.from_spin.value()
        toValue=self.ui.to_spin.value()
        samples=self.ui.samples_spin.value()
        sigma=self.ui.stdDev_spin.value()

        params=None
        dialog=None
        generatorObject=None

        if self.ui.sinus_radio.isChecked():
            dialog=SinusModelDialog.showDialog()
            params=dialog.getParameters()
            generatorObject=ModelFunctions.SinusLike(*params)

        elif self.ui.logarithm_radio.isChecked():
            dialog=LogModelDialog.showDialog()
            params=dialog.getParameters()
            generatorObject=ModelFunctions.LogLike(*params)

        elif self.ui.linear_radio.isChecked():
            dialog=LinearModelDialog.showDialog()
            params=dialog.getParameters()
            generatorObject=ModelFunctions.LinearLike(*params)

        l=Generator.generateRandom(generatorObject.countFromExternalValues,params,fromValue,toValue,samples,sigma)
        x,y=l

        model,error=Fitter.fitToModel(x,y)

        modelX=[(i*(toValue-fromValue)/10000)+fromValue for i in range(0,10000)]
        modelY=model.count(modelX)

        self.ui.plotterWidget.updatePlot(x,y,modelX,modelY)
        self.ui.statsWidget.updateResults(str(model),str(generatorObject),error,StatAnalyser.DeterminRatio(y,model.count(x)))

if __name__=="__main__":
    app = QApplication(sys.argv)
    w = Application()
    w.show()
    sys.exit(app.exec_())