# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'resultui.ui'
#
# Created: Tue Apr 19 22:41:07 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ResultUi(object):
    def setupUi(self, ResultUi):
        ResultUi.setObjectName("ResultUi")
        ResultUi.resize(868, 566)
        self.horizontalLayout = QtWidgets.QHBoxLayout(ResultUi)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label_3 = QtWidgets.QLabel(ResultUi)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 0, 0, 1, 1)
        self.label = QtWidgets.QLabel(ResultUi)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(ResultUi)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 3, 0, 1, 1)
        self.modelName = QtWidgets.QLabel(ResultUi)
        self.modelName.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.modelName.setText("")
        self.modelName.setWordWrap(True)
        self.modelName.setObjectName("modelName")
        self.gridLayout.addWidget(self.modelName, 1, 1, 1, 1)
        self.generatorName = QtWidgets.QLabel(ResultUi)
        self.generatorName.setMaximumSize(QtCore.QSize(10000000, 16777215))
        self.generatorName.setText("")
        self.generatorName.setWordWrap(True)
        self.generatorName.setObjectName("generatorName")
        self.gridLayout.addWidget(self.generatorName, 0, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(ResultUi)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 2, 0, 1, 1)
        self.phiValue = QtWidgets.QLabel(ResultUi)
        self.phiValue.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.phiValue.setText("")
        self.phiValue.setWordWrap(True)
        self.phiValue.setObjectName("phiValue")
        self.gridLayout.addWidget(self.phiValue, 2, 1, 1, 1)
        self.determinRatio = QtWidgets.QLabel(ResultUi)
        self.determinRatio.setText("")
        self.determinRatio.setObjectName("determinRatio")
        self.gridLayout.addWidget(self.determinRatio, 3, 1, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout)

        self.retranslateUi(ResultUi)
        QtCore.QMetaObject.connectSlotsByName(ResultUi)

    def retranslateUi(self, ResultUi):
        _translate = QtCore.QCoreApplication.translate
        ResultUi.setWindowTitle(_translate("ResultUi", "Form"))
        self.label_3.setText(_translate("ResultUi", "<b>Generator function:<b>"))
        self.label.setText(_translate("ResultUi", "<b>The best model: </b>"))
        self.label_2.setText(_translate("ResultUi", "<b>Determin ratio:</b>"))
        self.label_4.setText(_translate("ResultUi", "<b>Poison test</b>"))

