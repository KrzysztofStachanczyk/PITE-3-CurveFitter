# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'linearmodelui.ui'
#
# Created: Mon Apr 18 21:33:10 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_linearModelUi(object):
    def setupUi(self, linearModelUi):
        linearModelUi.setObjectName("linearModelUi")
        linearModelUi.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(linearModelUi)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtWidgets.QLabel(linearModelUi)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.label = QtWidgets.QLabel(linearModelUi)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label_3 = QtWidgets.QLabel(linearModelUi)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.a_param = QtWidgets.QDoubleSpinBox(linearModelUi)
        self.a_param.setMinimum(-10000.0)
        self.a_param.setMaximum(10000.0)
        self.a_param.setObjectName("a_param")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.a_param)
        self.label_4 = QtWidgets.QLabel(linearModelUi)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.b_param = QtWidgets.QDoubleSpinBox(linearModelUi)
        self.b_param.setMinimum(-10000.0)
        self.b_param.setMaximum(10000.0)
        self.b_param.setObjectName("b_param")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.b_param)
        self.verticalLayout.addLayout(self.formLayout)
        self.apply_button = QtWidgets.QPushButton(linearModelUi)
        self.apply_button.setObjectName("apply_button")
        self.verticalLayout.addWidget(self.apply_button)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)

        self.retranslateUi(linearModelUi)
        QtCore.QMetaObject.connectSlotsByName(linearModelUi)

    def retranslateUi(self, linearModelUi):
        _translate = QtCore.QCoreApplication.translate
        linearModelUi.setWindowTitle(_translate("linearModelUi", "Dialog"))
        self.label_2.setText(_translate("linearModelUi", "<b>Select parameters:</b>"))
        self.label.setText(_translate("linearModelUi", "y=ax + b"))
        self.label_3.setText(_translate("linearModelUi", "a"))
        self.label_4.setText(_translate("linearModelUi", "b"))
        self.apply_button.setText(_translate("linearModelUi", "Apply"))

