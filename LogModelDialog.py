from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from LogModelUi import Ui_logModelUi


class LogModelDialog(QDialog):
    """
    Dialog zbierajacy informacje do generatora modelu opartego o funkcje logarytmiczna.
    """

    def __init__(self, base=None):
        """
        Konstruktor ładujący UI i łączący sygnały ze slotami
        """
        super(LogModelDialog, self).__init__(base)
        self.ui=Ui_logModelUi()
        self.ui.setupUi(self)
        self.ui.apply_button.clicked.connect(self.onApply)

    def onApply(self):
        """
        Slot obsługujący przycisk "Apply"
        """
        self.accept()

    def getParameters(self):
        """
        Metoda zwracajaca parametry modelu ustawione w polach okna dialogowego
        w formacie y=a*log(bx+c)
        Zwraca:
            - krotka zawierajaca w.w. dane (współczynniki a,b,c)
        """
        self.params=(self.ui.a_param.value(),self.ui.b_param.value(),self.ui.c_param.value())
        return self.params


    @staticmethod
    def showDialog(base=None):
        """
        Metoda statyczna wyświetlajaca dialog
        """
        dialog = LogModelDialog(base)
        dialog.exec_()
        return dialog
