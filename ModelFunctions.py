from math import *
import numpy as np


class SinusLike:
    """
    Model oparty na funkcji sinus
    """

    def __init__(self,a,b,c):
        """
        Konstruktor tworzączy model w postaci y=a*sin(bx+c).
        Argumenty:
            w.w współczynniki
        """
        self.a=a
        self.b=b
        self.c=c

    def __str__(self):
        """
        Metoda wypisująca równanie modelu w postaci tekstowej
        Zwraca:
            odpowiednio sformatowany String
        """
        return "f(x)= "+str(round(self.a,4))+ "*sin("+str(round(self.b,4))+" x + "+str(round(self.c,4))+")"


    @staticmethod
    def countFromExternalValues(x,a,b,c):
        """
        Oblicza wartości na podstawie modelu i zewnętrznych danych
        Argumenty:
         x - lista argumentów do przetworzenia
         a,b,c - współczynniki dla modelu
        Zwraca:
            listę wartości obliczonych dla podanych danych
        """
        return a*np.sin(b*np.array(x)+c)


    def count(self,x):
        """
        Oblicza wartości na podstawie modelu i wewnętrznych danych
        Argumenty:
            x - lista argumentów do przetworzenia
        Zwraca:
            listę wartości obliczonych dla podanych danych
        """
        return SinusLike.countFromExternalValues(x,self.a,self.b,self.c)



class LogLike:
    """
    Model oparty na funkcji log
    """

    def __init__(self,a,b,c):
        """
        Konstruktor tworzączy model w postaci y=a*log(bx+c).
        Argumenty:
            w.w współczynniki
        """
        self.a=a
        self.b=b
        self.c=c


    def __str__(self):
        """
        Metoda wypisująca równanie modelu w postaci tekstowej
        Zwraca:
            odpowiednio sformatowany String
        """
        return "f(x)= "+str(round(self.a,4))+ "*log("+str(round(self.b,4))+" x + "+str(round(self.c,4))+")"



    @staticmethod
    def countFromExternalValues(x,a,b,c):
        """
        Oblicza wartości na podstawie modelu i zewnętrznych danych
        Argumenty:
            x - lista argumentów do przetworzenia
            a,b,c - współczynniki dla modelu
        Zwraca:
            listę wartości obliczonych dla podanych danych
        """
        return a*np.log(b*np.array(x)+c)


    def count(self,x):
        """
        Oblicza wartości na podstawie modelu i wewnętrznych danych
        Argumenty:
            x - lista argumentów do przetworzenia
        Zwraca:
            listę wartości obliczonych dla podanych danych
        """
        return LogLike.countFromExternalValues(x,self.a,self.b,self.c)


class LinearLike:
    """
    Model oparty na funkcji liniowej
    """
    def __init__(self,a,b):
        """
        Konstruktor tworzączy model w postaci y=ax+b.
        Argumenty:
            w.w współczynniki
        """
        self.a=a
        self.b=b


    def __str__(self):
        """
        Metoda wypisująca równanie modelu w postaci tekstowej
        Zwraca:
            odpowiednio sformatowany String
        """
        return "f(x)= "+str(round(self.a,4))+ " *x +"+str(round(self.b,4))


    @staticmethod
    def countFromExternalValues(x,a,b):
        """
        Oblicza wartości na podstawie modelu i zewnętrznych danych
        Argumenty:
            x - lista argumentów do przetworzenia
            a,b- współczynniki dla modelu
        Zwraca:
            listę wartości obliczonych dla podanych danych
        """
        return a*np.array(x)+b


    def count(self,x):
        """
        Oblicza wartości na podstawie modelu i wewnętrznych danych
        Argumenty:
            x - lista argumentów do przetworzenia
        Zwraca:
            listę wartości obliczonych dla podanych danych
        """
        return LinearLike.countFromExternalValues(x,self.a,self.b)


"""
Lista dostępnych modeli
"""
listOfModels=[LinearLike,LogLike,SinusLike]
