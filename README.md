# Projekt na przedmiot Python in the enterprise

### Cel:
Napisanie aplikacji potrafiącą dopasować wygenerowane dane do odpowiadającego im modelu. Program dodatkowo ma wykonywać ocenę stopnia dopasowania przy pomocy testu X^2 oraz wyświetlać graficzną reprezentację danych.

### Założenia wstępne:
- Jako źródło do generatora danych wykorzystane zostaną obsługiwane modele odpowiednio zaburzone przy pomocy wprowadzenia błędu o rozkładzie normalnym (najbardziej prawdopodobnym dla danych doświadczalnych).
- Budowa GUI umożliwiającego kontrolę parametrów generatora oraz wizualizację dopasowania.
- Aplikacja musi byś skalowalna, dodanie obsługi kolejnego modelu powinno wymagać zmiany jak najmniejszej ilości kodu.
Wykorzystanie elementów wzorca MVCS w programie.

### Wykorzystane biblioteki i pakiety:
- Numpy - wydajne przetwarzanie danych w tym operacje związane z obliczaniem statystyk oraz wyznaczaniem wartości funkcji modelowej na podstawie zadanych argumentów.
- PYQT5 - budowa graficznego interface-u użytkownika.
- mathplotlib - rysowanie wykresu dopasowania.
- matplotlib.backends.backend_qt5agg - możliwość osadzania wykresów z mathplotlib jako widgetów QT.
- scipy.optimize - wyznaczanie parametrów dopasowywanej do danych krzywej

### Wykorzystane narzędzia:
- QtCreator - tworzenie formularzy GUI
- pyuic5 - generowanie kodu pozwalającego na utworzenie rozkładu elementów GUI na podstawie plików .ui
- PyCharm - IDE do programowania w Pythonie
- SmartGit - graficzna nakładka na git-a

### Budowa generatora:
Generator jako swoje parametry przyjmuje wskaźnik na funkcje modelową (wraz z jej parametrami), zakres danych, ilość próbek oraz odchylenie standardowe wartości od krzywej modelu.

W pierwszym etapie losuje on zadaną ilość argumentów w wskazanym zakresie korzystając z funkcji generujących jednostajny rozkład wartości.

Następnie dla otrzymanych argumentów obliczane są wartości funkcji modelowej i tworzona jest tablica zawierająca błędy   o rozkładzie normalnym i odchyleniu zadanym przez użytkownika.

Ostatnim etapem jest dodanie tablic zawierających modelowe wartości i wygenerowane błędy.

W ten sposób można zakłócić dowolną funkcje modelową uzyskując wynik zbliżony do tego jakiego należy się spodziewać przy dokonywaniu pomiarów.

### Budowa elementu dopasowującego:
Fitter otrzymuje jako parametr listę argumentów i wartości próbek pochodzących z generatora. 

Następnie dla każdego modelu znajdującego się na liście w pliku ModelFunctions  oblicza on optymalne parametry (scipy.optimize.curve_fit) i dokonuje oceny zgodności z danymi (przy pomocy testu X^2).

Zwracany jest model z minimalna wartością funkcji błędu o obliczonych wcześniej parametrach oraz odpowiadający mu wynik testu X^2. 


### Podział plików i klas:
#### Service:
- Fitter - dostarcza funkcje pozwalającą wykonać dopasowanie danych do modelu.
- StatAnalyser - metody pozwalające ocenić jakość dopasowania (ChiSquared, DeterminRatio).
- Generator - tworzy losowe dane na podstawie zadanej funkcji modelowej (opisane powyżej).

#### Modele:
- ModelFunctions - zawiera zbiór klas reprezentujących różne funkcje modelowe.

#### Widoki:
- MainUi - główny widok menu dostarczający kontrolki dla generatorów oraz osadzający pozostałe widoki.
- ResultUi - reprezentuje dany dostarczające wybranej funkcji modelowej oraz stopnia jej dopasowania.
- LogModelUi/LinearModelUi/SinusModelUi - widoki odpowiedzialne za wyświetlanie kontrolek do zbierania parametrów konkretnej funkcji modelowej dla generatora (w osobnym oknie (dialogu)).

#### Kontrolery:
- Application - główny kontroler aplikacji zarządzający przepływem danych (wywołujący odpowiednie usługi i aktywujący inne kontrolery).
- LogModelDialog/LinearModelDialog/SinusModelDialog - kontrolery obsługujące widoki  do zbierania parametrów konkretnej funkcji modelowej dla generatora
- StatsWidget - kontroler sterujący aktualizacją danych na widoku ResultUi dotyczących statystyk dopasowania.
- Plotter - realizuje operacje rysowania wykresów dopasowania na bibliotecznym widoku klasy FigureCanvas.

### Podsumowanie:
Funkcjonalności opisane w założeniach projektu zostały zrealizowane. Otrzymana aplikacja rozpoznaje większość wygenerowanych modeli nawet dla dużych odchyleń. Zastosowanie biblioteki numpy umożliwiło uproszczenie kodu operującego na listach wartości. Warto byłoby się zastanowić nad poprawieniem kryterium wyboru właściwego modelu tak aby bazowało na więcej niż jednej statystyce. 

Przydatną opcją mogłoby być również zaimplementowanie odczytu danych z zewnętrznego źródła (pliku) celem ich analizy oraz zwiększenie ilości wspieranych modeli. Co dzięki modułowej budowie programu jest proste do dołączenia.

### Problemy
- Bilioteczna funkcja scipy.optimize.curve_fit nie zawsze radzi sobie z dopasowywaniem modelu funkcji sinus jeżeli w analizowanym zakresie znajduje się więcej niż jeden okres.

