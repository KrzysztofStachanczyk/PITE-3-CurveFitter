import numpy

class StatAnalyser:
    @staticmethod
    def DeterminRatio(ySampleData,yModelData):
        """
        Oblicza wskaźnik dopasowania danych do modelu oparty o współczynnik determinacji
        :param ySampleData: dane pochodzace z próbek
        :param yModelData: odpowiadajace im wartosci modelu
        :return:
        """
        ySampleData=numpy.array(ySampleData)
        yModelData=numpy.array(yModelData)
        yAverage=numpy.average(ySampleData)

        error1=yModelData-yAverage
        error1Powered=error1.T*error1

        error2=ySampleData-yAverage
        error2Powered=error2.T*error2

        result=numpy.sum(error1Powered)/numpy.sum(error2Powered)
        return result


    @staticmethod
    def ChiSquared(ySampleData,yModelData):
        """
        Przeprowadza X^2 test na zadanym zbiorze wartosci
        :param ySampleData: dane pochodzace z próbek
        :param yModelData: odpowiadajace im wartosci modelu
        :return: wynik testu
        """

        ySampleData=numpy.array(ySampleData)
        yModelData=numpy.array(yModelData)

        error1=ySampleData-yModelData
        error1Powered=error1.T*error1

        result=numpy.sum(error1Powered/numpy.abs(yModelData))
        return result