from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import *
from matplotlib.axes import *
from matplotlib.pyplot import *
from datetime import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

"""
Klasa odpowiedzialna za rysowanie wykresów z
"""


class Plotter(FigureCanvas):
    """
    Konstruktor inicjujacy podstawowe parametry wykresu
    i polityke skalowania
    """

    def __init__(self, parent=None, width=5, height=4, dpi=80):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.fig.autofmt_xdate()
        self.axes = self.fig.add_subplot(111)
        self.axes.hold(True)

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass

    """
    Slot aktualizujacy dane na wykresie
    Argumenty:
        xSamples,ySamples - zbiór punktów pochodzacych z probek
        xModel,yModel - zbiór punktów reprezentujacych model
    """

    def updatePlot(self, xSamples, ySamples, xModel, yModel):
        self.fig.delaxes(self.axes)

        self.axes = self.fig.add_subplot(111)
        self.axes.hold(True)

        self.axes.scatter(xSamples, ySamples, color="r", label="samples")

        self.axes.plot(xModel, yModel, color="g", label="model")

        self.axes.legend(loc=2, bbox_to_anchor=(0, 0.9), borderaxespad=0., fontsize='x-small', framealpha=0.25,
                         markerscale=0.5, ncol=1, numpoints=1)
        self.draw()
