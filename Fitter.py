from scipy.optimize import curve_fit
import numpy as np
import ModelFunctions
from StatAnalyser import StatAnalyser


class Fitter:

    @staticmethod
    def fitToModel(xData,yData):
        """
        Próbuje dopasować dane do jednego z istniejących modeli (z pliku ModelFunctions)
        Argumenty:
            xData-lista argumentów funkcji
            yData-lista wartości odpowiadającym argumentom
        Zwraca:
            obiekt modelu z ustawionymi parametrami
            wartość testu X^2 dla zadanego modelu
        """
        popt=None
        theBestModelClass=None
        theBestAlligment=None
        params=None

        for modelClass in ModelFunctions.listOfModels:
            try:
                popt , *pconv =curve_fit(modelClass.countFromExternalValues,xData,yData,method="trf")

                yModelData=modelClass.countFromExternalValues(xData,*(popt))
                alligment=StatAnalyser.ChiSquared(yData,yModelData)

                print(str(modelClass)+" : "+str(popt)+" : "+str(alligment))
                if theBestAlligment is None:
                    theBestAlligment=alligment
                    theBestModelClass=modelClass
                    params=popt

                elif theBestAlligment>alligment:
                    theBestAlligment=alligment
                    theBestModelClass=modelClass
                    params=popt

            except Exception:
                print("FAILED TO FIT: "+str(modelClass(0,0,0)))

        model=theBestModelClass(*params)

        return (model,theBestAlligment)
